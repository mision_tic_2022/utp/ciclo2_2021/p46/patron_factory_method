public class Cuadrado extends Figura {
    //Atributo
    private double lado;

    //Constructor
    public Cuadrado(double lado){
        this.lado = lado;
    }

    //Métodos
    public double getPerimetro(){
        return 4 * this.lado;
    }

    public double getArea(){
        return 2* this.lado;
    }
}
