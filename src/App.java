public class App {
    public static void main(String[] args) throws Exception {
        
        Figura objCuadrado = FabricaFiguras.crear_cuadrado(10);
        System.out.println("----------CUADRADO--------");
        System.out.println("Perimetro: "+objCuadrado.getPerimetro());
        System.out.println("Area: "+objCuadrado.getArea());

        Figura objCirculo = FabricaFiguras.crear_circulo(8);
        System.out.println("--------CIRCULO--------");
        System.out.println("Perimetro: "+objCirculo.getPerimetro());
        System.out.println("Area: "+objCirculo.getArea());

    }
}
